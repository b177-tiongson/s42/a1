const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const txtFullName = document.getElementsByClassName('txt-full-name')
const spanFullName = document.querySelector('#span-full-name')


//Alternative ways to retrieve elements
//document.getElementById('text-first-name')
//document.getElementByClassName('txt-inputs')
//document.getElementByTagName('input')


for(let x=0; x<txtFullName.length; x++){
	txtFullName[x].addEventListener('keyup',(event)=>{

		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`

		console.log(event.target)
		console.log(event.target.value)

	})
}

